import { animate } from "./setInterval.js";

function initSetInterval() {
	const btnStart = document.querySelector('#start');
	const elem = document.querySelector('.elem');


	btnStart.addEventListener('click', animate.bind(null, elem));
}

export { initSetInterval };
