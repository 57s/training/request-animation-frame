import { initSetInterval } from "./init-setInterval.js";
import { initSetTimeOut } from "./init-setTimeOut.js";
import { initRequestAnimationFrame_v1 } from "./init-requestAnimationFrame_v1.js";
import { initAnimate } from "./init-animate.js";
import { initRequestAnimationFrame_v2 } from "./init-animate_v2.js";

// initSetInterval();
// initSetTimeOut();
// initRequestAnimationFrame_v1();
// initAnimate()
initRequestAnimationFrame_v2()