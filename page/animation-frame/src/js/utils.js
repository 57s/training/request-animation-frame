function animate({ duration, drawProgress, endAnimation }) {
	let startAnimation = null;

	requestAnimationFrame(function measure(time) {
		if (!startAnimation) {
			startAnimation = time;
		}

		// progress = от 0 до 1, где 1 это 100% анимации
		const progress = (time - startAnimation) / duration;
		drawProgress(progress);

		// Если анимация выполнилась не на 100%, запустить следующую итерацию
		if (progress < 1) {
			requestAnimationFrame(measure);
		} else {
			endAnimation();
		}
	});
}

function animate_v2({ duration, fnTiming, fnDrawProgress, fnEndAnimation }) {
	const timeStart = performance.now();

	function animationIteration(time) {
		let step = (time - timeStart) / duration;
		const progress = fnTiming(step);
		fnDrawProgress(progress);

		if (step < 1) {
			requestAnimationFrame(animationIteration);
		} else {
			fnEndAnimation();
		}
	}

	requestAnimationFrame(animationIteration);
}

// Плавный разгон и торможение
function easeInOut(time) {
	return 0.5 * (1 - Math.cos(Math.PI * time));
}

function quad(time) {
	return Math.pow(time, 2);
}

export { animate, animate_v2, easeInOut, quad };
