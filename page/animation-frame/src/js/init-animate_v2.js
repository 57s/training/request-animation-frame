import { animate_v2, quad } from './utils.js';

function initRequestAnimationFrame_v2() {
	const btnStart = document.querySelector('#start');
	const polygon = document.querySelector('.polygon');
	const elem = document.querySelector('.elem');

	const elemPolygonWidth = polygon.offsetWidth;
	const elemWidth = elem.offsetWidth;
	const distance = elemPolygonWidth - elemWidth;

	function drawElement(progress) {
		let translate = progress * distance;

		// Fix смещения за полигон
		if (translate > distance) translate = distance;

		elem.style.transform = `translateX(${translate}px)`;
	}

	function endAnimation() {
		console.log('Анимация закончена');
	}

	const conf = {
		duration: 1000,
		fnTiming: quad,
		fnDrawProgress: drawElement,
		fnEndAnimation: endAnimation,
	};

	btnStart.addEventListener('click', animate_v2.bind(null, conf));
}

export { initRequestAnimationFrame_v2 };
