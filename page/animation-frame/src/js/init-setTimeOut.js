import { animate } from "./setTimeOut.js";

function initSetTimeOut() {
	const btnStart = document.querySelector('#start');
	const elem = document.querySelector('.elem');


	btnStart.addEventListener('click', animate.bind(null, elem));
}

export { initSetTimeOut };
