const conf = {
	duration: 5,
	iterations: 100,
	polygon: {
		width: 700,
		height: 50,
	},
	elem: {
		width: 50,
		height: 50,
	},
};

function animate(elem) {
	const timeIteration = (conf.duration / conf.iterations) * 1000;
	let step = 0;

	function drawProgress(progress) {
		const offset = (conf.polygon.width - conf.elem.width) * progress;
		elem.style.transform = `translateX(${offset}px)`;
	}

	function go() {
		step++;
		const progress = step / conf.iterations;
		drawProgress(progress);

		if (step < conf.iterations) {
			setTimeout(go, timeIteration);
		}
	}

	go()
}

export { animate };
