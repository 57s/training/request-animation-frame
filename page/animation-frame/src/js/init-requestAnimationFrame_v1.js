import { animate } from "./requestAnimationFrame_v1.js";
function initRequestAnimationFrame_v1() {
	const btnStart = document.querySelector('#start');
	const elem = document.querySelector('.elem');

	btnStart.addEventListener('click', animate.bind(null, elem));
}

export { initRequestAnimationFrame_v1 };
