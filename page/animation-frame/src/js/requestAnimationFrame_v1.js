function animate(element) {
	const duration = 1000;
	// Дистанция полигона. Ширина полигона - ширина элемента
	const distance = 700 - 50;
	let startAnimation = null;

	requestAnimationFrame(function measure(time) {
		if (!startAnimation) {
			startAnimation = time;
		}

		// progress = от 0 до 1, где 1 это 100% анимации
		const progress = (time - startAnimation) / duration;
		//
		let translate = progress * distance;

		// Fix смещения за полигон
		if (translate > 650) translate = 650;

		element.style.transform = `translateX(${translate}px)`;

		// Если анимация выполнилась не на 100%, запустить следующую итерацию
		if (progress < 1) {
			requestAnimationFrame(measure);
		}
	});
}

export { animate };
