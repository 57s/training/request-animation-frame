const conf = {
	duration: 5,
	iterations: 10,
	polygon: {
		width: 700,
		height: 50,
	},
	elem: {
		width: 50,
		height: 50,
	},
};

function animate(elem) {
	const timeIteration = (conf.duration / conf.iterations) * 1000;
	let step = 0;

	function drawProgress(progress) {
		const offset = (conf.polygon.width - conf.elem.width) * progress;
		elem.style.transform = `translateX(${offset}px)`;
	}

	const intervalId = setInterval(() => {
		step++;

		if (step >= conf.iterations) {
			clearInterval(intervalId);
		}

		const progress = step / conf.iterations;
		drawProgress(progress);
	}, timeIteration);
}

export { animate };
