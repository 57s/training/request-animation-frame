import { animate, easeInOut } from './utils.js';

function initAnimate() {
	const btnStart = document.querySelector('#start');
	const polygon = document.querySelector('.polygon');
	const elem = document.querySelector('.elem');

	function drawElement(progress) {
		const elemPolygonWidth = polygon.offsetWidth;
		const elemWidth = elem.offsetWidth;
		const distance = elemPolygonWidth - elemWidth;
		let translate = easeInOut(progress) * distance;
		// Fix смещения за полигон
		if (translate > distance) translate = distance;

		elem.style.transform = `translateX(${translate}px)`;
	}

	function endAnimation() {
		console.log('Анимация закончена');
	}

	const conf = { duration: 5000, drawProgress: drawElement, endAnimation };
	btnStart.addEventListener('click', animate.bind(null, conf));
}

export { initAnimate };
